#!/usr/bin/env sh

[[ -f ~/.Xresources ]] && xrdb -load $HOME/.Xresources
[[ -f ~/.Xmodmap ]] && xmodmap ~/.Xmodmap

xset +fp ~/.fonts
xset fp rehash

# faster key repeat rate and shorter delay (default is somewhere at 600 25)
xset r rate 300 35

# slow the mouse down a wee bit
xinput --set-prop 9 'libinput Accel Speed' -0.35

MONITOR_MAIN="DVI-D-0"
MONITOR_LEFT="DisplayPort-2"
MONITOR_RIGHT="HDMI-A-0"
export MONITOR_{MAIN,LEFT,RIGHT}

# 96Hz for the QNIX QX2710 (YMMV; this particular monitor doesn't play nice with 100Hz)
# Thanks <http://blog.patshead.com/2014/12/overclocking-my-qnix-qx2710-monitors-with-linux-and-xorg.html>
xrandr --newmode "2560x1440@96" 400.00 2560 2608 2640 2744 1440 1443 1448 1512 +hsync +vsync
xrandr --addmode "$MONITOR_MAIN" "2560x1440@96"

# Generated via arandr
~/.xlayout

# delay screensaving time to 2h
xset -dpms
xset s 7200 7200

wm() {
  feh --bg-tile ~/media/wallpapers/patterns/blue.png
  xsetroot -cursor_name left_ptr &
  unclutter &
  dunst --config ~/.config/dunst/dunstrc.desktop &
  compton --config ~/.config/compton.desktop.conf &
  redshift &
  polybar main-bot &

  wm=$1
  sxhkd -c $HOME/.config/sxhkd/{sxhkdrc.desktop,commons} &

  wmname LG3D

  $wm
}

wm bspwm
