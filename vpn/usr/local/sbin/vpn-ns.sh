#!/usr/bin/env bash

function iface_up() {
    sysctl -q net.ipv4.ip_forward=1
    ip netns add vpn
    ip link add veth0 type veth peer name eth0
    ip link set eth0 netns vpn
    ip addr add 10.0.0.1/24 dev veth0
    ip netns exec vpn ip addr add 10.0.0.2/24 dev eth0
    ip link set veth0 up
    ip netns exec vpn ip link set eth0 up

    ip netns exec vpn ip route add default via 10.0.0.1 dev eth0
    ip netns exec vpn ip addr add 127.0.0.1 dev lo
    ip netns exec vpn ip link set lo up

    iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -d 0.0.0.0/0 -j MASQUERADE
}

function iface_down() {
    sysctl -q net.ipv4.ip_forward=0
    iptables -t nat -D POSTROUTING -s 10.0.0.0/24 -d 0.0.0.0/0 -j MASQUERADE
    ip netns delete vpn
}

case "$1" in
    up)   iface_up   ;;
    down) iface_down ;;
    *)
        echo "Syntax: $0 up|down|run"
        exit 1
    ;;
esac

