The directory `usr/local/sbin` and its content is copied to `/` with `stow -t / vpn`.

The user-run `bin/vpn` runs `/usr/bin/locan/sbin/vpn-run.sh` with `sudo` to start a program within the network namespace `vpn`.

`vpn-run.sh` can be run as root without the requirements of entering a password with this `/etc/sudoers` rule: `tmplt ALL = (root) NOPASSWD: /usr/local/sbin/vpn-run.sh`. I am the only user on the system, so I have hardcoded my username.

To setup this namespace, `vpn-ns.sh up` and an OpenVPN session within it is run at system start.
