import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
import XMonad.Hooks.EwmhDesktops
import XMonad.Util.Run
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Util.EZConfig(additionalKeysP)
import XMonad.Layout.NoBorders
import XMonad.Layout.Fullscreen hiding (fullscreenEventHook)
import XMonad.Layout.LayoutHints
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import System.IO
import System.Exit
import Data.List

import qualified XMonad.StackSet as W

myTerminal    = "termite"
myModMask     = mod4Mask
myWorkspaces  = ["1:term","2:web","3:vidya","4:vm","5:media"] ++ map show [6..9]
myStatusBar   = "xmobar ~/.xmonad/xmobar.hs"

myManageHook = composeAll
    [ isFullscreen               --> doFullFloat
    , className =? "Firefox"     --> doShift "2:web"
    , className =? "Gimp"        --> doFloat
    , className =? "Steam"       --> doFloat <+> doShift "3:vidya"
    , className =? "mpv"         --> doCenterFloat
    , className =? "Wine"        --> doFloat <+> doShift "3:vidya"
    , className =? "Psychonauts" --> doFloat
    , className =? "Teamspeak 3" --> doShift "1:term"

    , className =? "syncsim-SyncSim" --> doFloat

    , title =? "ranger"     --> doCenterFloat
    , title =? "passwdmngr" --> doCenterFloat -- rewrite to make use of xdo
    , title =? "irssi"      --> doShift "1:term"
    , manageHook desktopConfig
    ]

-- look into @altercation's use of subkeys
myKeys =
    -- System / Utilities
    [ ("M-q",            safeSpawn "xmonad" ["--restart"])
    , ("M-C-q",          spawn "xmonad --recompile && xmonad --restart")
    , ("M-S-q",          confirmPrompt hotPromptTheme "Quit XMonad" $ io (exitWith ExitSuccess))
    , ("M-x",            safeSpawnProg "lock")
    , ("M-S-c",          kill)
    , ("M-n",            refresh)
    , ("M-b",            sendMessage ToggleStruts)
    -- Launching Programs
    , ("M-p",            safeSpawn "rofi" ["-show", "run"])
    , ("M-o",            safeSpawn "rofi" ["-show", "window"])
    , ("M-S-<Return>",   safeSpawnProg myTerminal)
    -- Launchig terminal programs
    , ("M-S-i",          safeSpawn myTerminal ["--title=irssi", "--exec=irssi"])
    , ("M-S-f",          safeSpawn myTerminal ["--title=ranger", "--exec=ranger"])
    , ("M-S-p",          safeSpawn myTerminal ["--title=passwdmngr", "--exec=fpass"])
    -- Actions
    , ("<Print>",        spawn "~/bin/scripts/screenshot.sh")
    -- Multimedia keys
    , ("M1-<Pause>",     safeSpawn "mpc" ["-q", "toggle"])
    , ("M1-<F11>",       safeSpawn "mpc" ["-q", "prev"])
    , ("M1-<F12>",       safeSpawn "mpc" ["-q", "next"])
    , ("M1-<Page_Up>",   safeSpawn "mpc" ["-q", "volume", "+1"])
    , ("M1-<Page_Down>", safeSpawn "mpc" ["-q", "volume", "-1"])
    , ("M1-z",           safeSpawn "mpc" ["-q", "random"])
    , ("M1-S-r",         safeSpawn "mpc" ["-q", "consume"])
    , ("M1-y",           safeSpawn "mpc" ["-q", "single"])
    , ("M1-r",           safeSpawn "mpc" ["-q", "repeat"])
    -- Layouts
    , ("M-<space>",      sendMessage NextLayout)
    -- Floating layer stuff
    , ("M-t",            withFocused $ windows . W.sink)
    -- Focus
    , ("M-<Tab>",        windows W.focusDown)
    , ("M-j",            windows W.focusDown)
    , ("M-k",            windows W.focusUp)
    , ("M-m",            windows W.focusMaster)
    -- Swapping
    , ("M-<Return>",     windows W.shiftMaster)
    , ("M-S-j",          windows W.swapDown)
    , ("M-S-k",          windows W.swapUp)
    -- Master windows count
    , ("M-.",            sendMessage (IncMasterN 1))
    , ("M-,",            sendMessage (IncMasterN (-1)))
    -- Resizing
    , ("M-h",            sendMessage Shrink)
    , ("M-l",            sendMessage Expand)
    -- Maximize window
    , ("M-f",            sendMessage $ Toggle FULL)
    ]

myStartupHook = do
    setWMName "LG3D" -- thanks, Java

main = do
    xmproc <- spawnPipe myStatusBar
    xmonad $ ewmh myConfig
        { logHook = dynamicLogWithPP xmobarPP
                    { ppOutput  = hPutStrLn xmproc
                    , ppTitle   = xmobarColor xmobarTitleColor "" . shorten 100
                    , ppCurrent = xmobarColor xmobarCurrentWorkspaceColor ""
                    , ppUrgent  = xmobarColor "#198844" ""
                    , ppSep     = "  ::  "
                    }
        } `additionalKeysP` myKeys

-- main = xmonad =<< statusBar myBar myPP toggleStrutsKey (ewmh $ myConfig)-- `additionalKeysP` myKeys

toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

myConfig = desktopConfig
    { -- simple stuff
      terminal           = myTerminal
    , modMask            = myModMask
    , borderWidth        = myBorderWidth
    , workspaces         = myWorkspaces
    , normalBorderColor  = base00
    , focusedBorderColor = blue

      -- TODO: make this works
      -- keys = (`additionalKeys` myKeys)

      -- hooks, layouts
    , layoutHook         = mkToggle (NOBORDERS ?? FULL ?? EOT) $ (smartBorders (avoidStruts $ layoutHook desktopConfig))
    , manageHook         = manageDocks <+> myManageHook
    , handleEventHook    = handleEventHook desktopConfig <+> fullscreenEventHook
    , startupHook        = myStartupHook
}

-- Colors and borders
myNormalBorderColor         = "#CCCCCC"
myFocusedBorderColor        = "#FFFFFF"
xmobarTitleColor            = "#b58900"
xmobarCurrentWorkspaceColor = "#859900"
myBorderWidth               = 3

-- Theming
base03  = "#002b36"
base02  = "#073642"
base01  = "#586e75"
base00  = "#657b83"
base0   = "#839496"
base1   = "#93a1a1"
base2   = "#eee8d5"
base3   = "#fdf6e3"
yellow  = "#b58900"
orange  = "#cb4b16"
red     = "#dc322f"
magenta = "#d33682"
violet  = "#6c71c4"
blue    = "#268bd2"
cyan    = "#2aa198"
green   = "#859900"

-- sizes
gap     = 10
topbar  = 10
border  = 0
prompt  = 20
status  = 20

active       = blue
activeWarn   = red
inactive     = base02
focusColor   = blue
unfocusColor = base02

myFont = "xft:Terminus:pixelsize=14:antialias=true:hinting=true"

myPromptTheme = def
    { font                  = myFont
    , bgColor               = base03
    , fgColor               = active
    , fgHLight              = base03
    , bgHLight              = active
    , borderColor           = base03
    , promptBorderWidth     = 0
    , height                = prompt
    , position              = Top
    }

hotPromptTheme = myPromptTheme
    { bgColor               = red
    , fgColor               = base3
    , position              = Top
    }
