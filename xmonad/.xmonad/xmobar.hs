Config { font             = "xft:Terminus:pixelsize=14:antialias=true:hinting=true"
       , additionalFonts  = [ "xft:FontAwesome:pixelsize=14:antialias=true:hinting=true"]
       , allDesktops      = True
       , bgColor          = "#000000"
       , fgColor          = "#586e75"
       -- , alpha            = 100
       , position         = TopW L 100
       , overrideRedirect = True
       , commands         = [
            Run Cpu
                [ "-t","<fn=1>\xf085</fn> C/M <vbar>"
                --, "-p", "2"
                , "-L", "40"
                , "-H", "60"
                , "-l", "#586e75"
                , "-h", "#dc322f" -- red
                ] 10
          , Run Memory
                [ "-t", "<usedvbar>"
                , "-p", "2"
                , "-l", "#586e75"
                , "-h", "#268bd2" -- blue, just to differentiate from cpu bar
                ] 10
          , Run Date "<fc=#268bd2><fn=1>\xf073</fn> %a %_d %b %Y | d.%j w.%W</fc>   <fc=#2AA198><fn=1></fn> %H:%M:%S</fc>" "date" 10
          , Run StdinReader
       ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "%StdinReader% }{ %cpu% | %memory%    <fc=#ee9a00>%date%</fc>"
       }
