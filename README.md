```
         ██            ██     ████ ██  ██
        ░██           ░██    ░██░ ░░  ░██
        ░██  ██████  ██████ ██████ ██ ░██  █████   ██████
     ██████ ██░░░░██░░░██░ ░░░██░ ░██ ░██ ██░░░██ ██░░░░
    ██░░░██░██   ░██  ░██    ░██  ░██ ░██░███████░░█████
   ░██  ░██░██   ░██  ░██    ░██  ░██ ░██░██░░░░  ░░░░░██
   ░░██████░░██████   ░░██   ░██  ░██ ███░░██████ ██████
    ░░░░░░  ░░░░░░     ░░    ░░   ░░ ░░░  ░░░░░░ ░░░░░░

     ▓▓▓▓▓▓▓▓▓▓▓▓
    ░▓ about    ▓ custom linux config files
    ░▓ author   ▓ tmplt (and a lot of other people whom I've stolen dots from)
    ░▓ code     ▓ https://github.com/Tmplt/dotfiles
    ░▓▓▓▓▓▓▓▓▓▓▓▓
    ░░░░░░░░░░░░


    Here are my dots which I use on my desktop and laptop.
        For options which I don't want across both systems (such as bspwm, sxhkd,
    mpv, etc.), I check the hostname and load the appropriate
    config.
        On GitHub I create issues for whatever is wrong with my setup, and of
    course try to resolve them. It's also a nice way to keep track of what I need
    to fix.
        Files containing secrets are encrypted with help of git-crypt
    <https://www.agwa.name/projects/git-crypt/>.
        To see existing file conflicts if you were to link these files:

            stow */ -t "$HOME" -n 2>&1 | grep -oE ":.+" | cut -c3-
```
---
```
    beets          > my config for sorting my music
    bin            > useful scripts and programs
    bspwm          > bspwm and lemonbuddy config
    compton        > minimal composite config for window shadows
    dunst          > minimal notification manager
    gdb            > a modular visual interface I copied
    git            > global git config
    irssi          > nixers irc theme
    latexmk        > config for automatically do a lot of stuff
    mpd            > MPD settings, and my playlists
    mpv            > higer-end-ish config
    mutt           > my email setup, ̶w̶i̶t̶h̶o̶u̶t with encrypted secrets
    ncmpcpp        > ncurses mpc++ ui/color/bindings settings
    neofetch       > the new screenfetch
    polybar        > my bar configurations
    pygments       > add syntax highlighting to cat and less commands
    qemu           > configs for my virtual machines (incl. GPU-passthrough)
    ranger         > file manager with image previews
    redshift       > for my eyes
    rtorrent       > efficient configs for Linux ISO downloads
    sxhkd          > for bspwm
    termite        > some nice colours
    tmux           > terminal multiplexer with custom status bar
    urxvt          > sourcerer terminal colors and keyboard settings
    vim            > but it's actually for neovim
    vpn            > scripts for a netns only for openvpn connections
    weechat        > not much to look at (yet)
    xfiles         > systemwide settings and Xresources
    xmonad         > sweet Haskell
    zathura        > black theme (however, image colours are inverted :c)
    zsh            > zshell settings, aliases (zim not included)
```
