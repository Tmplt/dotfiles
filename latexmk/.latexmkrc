## Export as pdf..
$pdf_mode = 1;

## .. and use zathura to read it.
## Unsure if this variable affects LaTeX-Box
#$pdf_previewer = "open -a /bin/zathura";
#$previewer = "open -a /bin/zathura";

## Automatically compile glossaries
add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

## Supress glossary output if --silent
sub run_makeglossaries {
  if ( $silent ) {
    system "makeglossaries -q '$_[0]'";
  }
  else {
    system "makeglossaries '$_[0]'";
  };
}

## Rules for glossary cleanup
push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';
$clean_ext .= ' %R.ist %R.xdy';
