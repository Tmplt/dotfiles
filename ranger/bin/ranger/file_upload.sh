#!/usr/bin/env bash
# Required: pbpst

echo_ranger()
{
    for instance in /tmp/ranger-ipc.*; do
        echo "$@" >> $instance
    done
}

type pbpst >/dev/null 2>/dev/null || {
    echo_ranger "echo Couldn't find pbpst, which is required."
    exit 1
}

url=$(pbpst -Sf "$1" -x 3d)
if [ $? -eq 0 ]; then
    xsel -c
    echo "$url" | xsel -pbi
    echo_ranger "echo '$1' uploaded, link copied to clipboard"
else
    echo_ranger "Unable to upload: $url"
fi

