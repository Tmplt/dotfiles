This directory contains related configuration files for my virtual machines.
At present, there is only the Windows vm, which I mostly to play games on.

---

# Previous issues with this setup
*   **Stuttering audio in higher-end games**:
    The host system has a single i5-6600k CPU.
    Even with it overclocked (albeit with the motherboard's own preset)
    the host gets somewhat strangled in CPU-heavy games (such as Battlefield 1).

    This was mostly caused by the VM pinning all CPU four CPU cores (bad idea; a *lot* of context switches).
    I curently only pin CPU 0 and 1.
    Games like *Rising Storm 2: Vietnam* and *Divinity: Original Sin 2* run without audio issues.
    I have yet to try any CPU-heavier games.

    Performance is still shoddy, but I believe the dedicated R9 290 is reaching it's EOL —
    on higher resolutions, the framerate is a perfect sine wave, to my bafflement.
    This even occurs on bare-metal Windows.

    Nevertheless, the following thread might be of interest when it comes to CPU pinning/isolation:
    <https://www.redhat.com/archives/vfio-users/2017-February/msg00010.html>.

*   Passing the mouse to the VM
Thanks to [Rgnk84 on /r/vfio](https://www.reddit.com/r/VFIO/comments/5jaibg/how_do_you_pass_keyboard_and_mouse_to_the_vm/dbkv84s/) I've finally ditched Synergy and the unreliable linux-input qemu module.
I have a keyboard dedicated to the VM but the mouse is something I share between host and quest.
Using the bind <C-S-m> on my host keyboard, I toggle the mouse attachment during VM runtime.
The script (`bin/vm-toggle-attach`) can easily be extended by adding additional .xml files ala `qemu/home/tmplt/.config/vm/devs/mouse.xml`.

# Current issues with this setup
*   **Overall bad game performance**:
    See above regarding the GPU; I really need a new one.
    If I could, I would interchange the cards on the motherboard,
    but the 3rd-party Arctic cooler is giganormous in every sense of the word.

    My motherboard also does not seem to be able to switch the two PCI-E slots.

    I'm also in the process of acquiring a CPU with hyper-threading.
    This, alongside a (maybe) Windows 10 installation and updated configuration, should hopefully increase overall performance.

*   **My monitor sucks**: All of them, really.
    While I want to play on my big main monitor, it only has a single DVI-D input,
    and I can't for the life of me find a DVI-D switch that can manage 1440p at 96Hz. All are locked at 1080p at 60Hz for some bloody reason.

    So for now, I'm currently playing on a small, boring screen that isn't centered on the desktop.
    As a plus, it's absolutely worthless when it comes to representing any colour (esp. shades of black) and there is a *ton* of ghosting.

    If you happen to stumble upon a switch that can manage this without costing an arm and a leg, please don't hesitate to send me an email.
    I'll owe you a beer.
