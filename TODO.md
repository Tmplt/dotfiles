# bspwm
* **super + alt + {q,w,e}**: switch desktops with another monitor;
* **super + shift + {q,w,e}**: move window to another monitor;
* **super + space**: change window layout, ala xmonad.
