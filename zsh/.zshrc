# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# source zim
if [[ -s ${ZDOTDIR:-${HOME}}/.zim/init.zsh ]]; then
  source ${ZDOTDIR:-${HOME}}/.zim/init.zsh
fi

# source configs
for config (~/.zsh/*.zsh) source $config
