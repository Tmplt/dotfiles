# TODO: check if the file was actually changed/written?

_open() {
  $EDITOR "$1"
}

conf() {
  [ -z $1 ] && return 1
  local confpath=${XDG_CONFIG_HOME:-~/.config}

  case $1 in
    zsh)   _open ~/.zsh/ && source ~/.zshrc ;;
    tmux)  _open ~/.tmux.conf ;;
    vim)   _open ~/.vimrc ;;
    git)   _open ~/.gitconfig ;;
    sxhkd) _open "$confpath/sxhkd" && pkill -USR1 -x sxhkd ;;
    mutt)  _open ~/.muttrc ;;
    irssi) _open ~/.irssi/config ;;
    hlwm)  _open ~/.config/herbstluftwm ;;

    redshift) _open "$confpath/redshift.conf" ;;
    rtorrent) _open "$HOME/.rtorrent/" ;;

    *)  [ -d "$confpath/$1" ] && _open "$confpath/$1"   &&
        printf "$1 has been altered but not sourced.\n" ||
        return 2 ;;
  esac
}
