# aliases
# # # # #
alias mntphone='simple-mtpfs ~/phone'
alias umntphone='fusermount -u ~/phone'
alias ytb='youtube-viewer'
alias xsel='xsel -b'
alias grep='grep --color=auto'
alias cp='rsync -ahX --info=progress2'
alias scp='scp -r'
alias week='date +%W'
alias top='glances'
alias e='nvim'
alias vim='nvim'
alias rmexif='exiftool -all='
alias x='cd && startx'
alias la='ls++ -a'
alias mixer='pulsemixer'
alias slideshow='feh --recursive --scale-down --randomize --auto-zoom --image-bg black &>/dev/null' # It's for my family photos, I promise!
alias grp='git remote | xargs -L1 git push --all' # Push to all removes
alias du='cdu -idh' # du är ganska najs
alias fort='fortune -a'
alias hack='cat /dev/urandom | hexdump -c'
alias pls='sudo $(fc -ln -1)' # Run previous command with sudo
alias ls='ls -F --color=auto'
alias ll='/usr/bin/vendor_perl/ls++'
alias mkdir='mkdir -p'
alias rmrf='rm -rf'
alias ag="ag --color --color-line-number '0;35' --color-match '46;30' --color-path '4;36'"
alias tree='tree -CAFa -I "CVS|*.*.package|.svn|.git|.hg|node_modules|bower_components" --dirsfirst'
alias rock='ncmpcpp'
alias matrix="cmatrix -b"
alias tempwatch="watch sensors"
alias toiletlist='for i in ${TOILET_FONT_PATH:=/usr/share/figlet}/*.{t,f}lf; do j=${i##*/}; echo ""; echo "╓───── "$j; echo "╙────────────────────────────────────── ─ ─ "; echo ""; toilet -d "${i%/*}" -f "$j" "${j%.*}"; done'
alias ascii="toilet -t -f 3d"
alias future="toilet -t -f future"
alias rusto="toilet -t -f rusto"
alias rustofat="toilet -t -f rustofat"
alias systemctl='sudo systemctl'
alias disks='echo "╓───── m o u n t . p o i n t s"; echo "╙────────────────────────────────────── ─ ─ "; lsblk -a; echo ""; echo "╓───── d i s k . u s a g e"; echo "╙────────────────────────────────────── ─ ─ "; df -h;'
alias steamwdl='steamcmd +@sSteamCmdForcePlatformType windows +force_install_dir "$(pwd | sed 's/ /\\ /g')"' # download a windows release of a game to the current directory
alias ren='ranger'
alias mount='udevil mount'
alias umount='udevil umount'
alias wtf='dmesg | tail -n 50'
alias gtr='translate.sh'
alias define='sdcv'
alias keybase='KEYBASE_PINENTRY=none keybase'
alias keybaseclear='keybase ctl app-exit; keybase ctl stop'
alias ytdl='youtube-dl --output "%(uploader)s - %(title)s.%(ext)s"'
alias tzat='tzathura &>/dev/null'
alias mps='mpsyt'
alias rt2days="rtcontrol -scompleted -ocompleted,is_open,up.sz,ratio,alias,name completed=-2d"
alias rtls="rtcontrol -qo '{{chr(10).join([d.directory+chr(47)+x.path for x in d.files])|h.subst(chr(47)+chr(43),chr(47))}}'"
alias restoremonitors="xrandr --output DVI-D-0 --primary --mode 2560x1440@96 --output DisplayPort-2 --mode 1920x1080 --left-of DVI-D-0 --rotate left"
alias hc="herbstclient"
alias lsd="exa -bghl --group-directories-first --sort name"

# rtorrent lives in FreeNAS system -> rtorrent jail -> admin user -> dtach session (WTF?)
# (Can we pipe the admin passwd into this from pass(1)?
alias rtorrent="ssh -t dulcia sudo jexec rtorrent \"su - admin -c 'dtach -a /tmp/rtorrent.sock'\""

# functions
# # # # # #

cat_color() {
  for file in "$@"; do
    pygmentize -O style=monokai -f console256 -g "$file"
  done
}
alias dog='cat_color'

less_color() {
  if [ $# -gt 0 ]; then
    pygmentize -O style=monokai -f console256 -g $1 | less -r
  fi
}
alias l='less_color'

# read markdown files like manpages
markdown_as_manpage() {
    pandoc -s -f markdown -t man "$*" | man -l -
}
alias md='markdown_as_manpage'

twitch () {
    SEC="${2}"
    streamlink --http-header Client-ID=jzkbprff40iqj646a697cyrvl0zt2m6 \
               --player=mpv "twitch.tv/$1" "${SEC:-source}"
}

translate() {
  dict -d fd-$1-$2 "${@:2}"
}
alias tra='translate'

waifu2x_upscale() {
  waifu2x -i "$1" -o "u_$1" >/dev/null
}
alias w2x='waifu2x_upscale'

mkcd() {
  mkdir -p "$1" && cd "$1"
}

setsteam() {
  steam "steam://friends/status/$1"
}

mkphonebackup() {
  aname="/tmp/mako-$(date +%Y-%m-%d).tar.xz"
  apack "$aname" "$1"
  gpg --recipient "password" --encrypt "$aname"
  scp "$aname.gpg" palaven:~/manual\ backups/
}

virsh() {
  # Don't require sudo if the user is in the right groups (kvm I think).
  command virsh -c qemu:///system "${@}"

  # Start a synergy client if a specific VM is started.
  if [[ $? -eq 0 ]] && [[ ${1} == "start" ]] && [[ ${2} == "windows" ]]; then
    #synergyc -f iskierka &>/dev/null &
  fi
}
