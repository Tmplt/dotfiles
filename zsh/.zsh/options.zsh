setopt   interactivecomments
setopt   HIST_EXPIRE_DUPS_FIRST
setopt   nonomatch # forward wildcard as argument if no match
unsetopt correct   # don't guess my misspellings
