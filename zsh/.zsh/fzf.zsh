type fzf 2>&1 >/dev/null || {
  printf "fzf is not installed.\n" >&2
  return
}

# Auto-completion
# ---------------
source "/home/tmplt/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/tmplt/.fzf/shell/key-bindings.zsh"

# fe [FUZZY PATTERN] - Open the selected file with the default editor
#   - Bypass fuzzy finder if there's only one match (--select-1)
#   - Exit if there's no match (--exit-0)
fe() {
  local files
  IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

# Modified version where you can press
#   - CTRL-O to open with `open` command,
#   - CTRL-E or Enter key to open with the $EDITOR
fo() {
  local out file key
  IFS=$'\n' out=($(fzf-tmux --query="$1" --exit-0 --expect=ctrl-o,ctrl-e))
  key=${out[0]}
  file=${out[1]}
  if [ -n "$file" ]; then
    [ "$key" = ctrl-o ] && open "$file" || ${EDITOR:-vim} "$file"
  fi
}

# fd - cd to selected directory
fd() {
  local dir
  dir=$(find -L ${1:-.} -path '*/\.*' -prune \
                  -o -type d -print 2> /dev/null | fzf +m) &&
  cd "$dir"
}

# fda - including hidden directories
fda() {
  local dir
  dir=$(find -L ${1:-.} -type d 2> /dev/null | fzf +m) && cd "$dir"
}

# fh - repeat history
fh() {
  print -z $( ([ -n "$ZSH_NAME" ] && fc -l 1 || history) | fzf +s --tac | sed 's/ *[0-9]* *//')
}

unalias z 2> /dev/null
z() {
  [ $# -gt 0 ] && _z "$*" && return
  cd "$(_z -l 2>&1 | fzf-tmux +s --tac --query "$*" | sed 's/^[0-9,.]* *//')"
}

# fmpc - find and play a track in the current mpd playlist
fmpc() {
  local song_position
  song_position=$(mpc -f "%position%) %artist% - %title%" playlist | \
    fzf --query="$1" --reverse --select-1 --exit-0 | \
    sed -n 's/^\([0-9]\+\)).*/\1/p') || return 1
  [ -n "$song_position" ] && mpc -q play $song_position
}

# sampc - search for and add mpd album(s) to the current playlist
sampc() {
  local albums

  # ${(@f)... - split string into array at newlines
  albums=("${(@f)$(mpc list album | \
    fzf -m --reverse --select-1 --exit-0)}") || return 1

  for album ($albums) mpc -q findadd album "$album"
}

# fkill - lazy fzf function to kill processes
fkill() {
  local pid=$(ps ax | sed '1d' | fzf --tac | awk '{print $1}')
  ! [ -z pid ] && sudo kill -9 $pid
}
