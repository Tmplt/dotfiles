#!/usr/bin/env bash

path="/tmp/maim_screenshot.png"

# Slap on a cool red border, there
maim -s -c 1,0,0,0.6 $path &>/dev/null

if [ -e $path ]; then
    url=$(pbpst -Sf $path -x 1d)
    if [ $? -eq 0 ]; then
        xsel -c
        notify-send "Uploaded image has been copied to clipboard"
        echo "$url" | xsel -pbi
    else
        notify-send "Unable to upload: $url"
    fi
    rm $path
fi

