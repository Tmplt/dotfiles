### rtorrent settings #######################################################
#
# This is the standard configuration that supports both PyroScope and
# (optionally) rTorrent-PS features.
#
# It is installed by Ansible, but only gets overwritten when you provide the
# option "-e force_cfg=yes" to the "ansible-playbook" command, after the
# initial playbook run.
#
# For more info regarding changing configuration defaults, see the README:
#
#   https://github.com/pyroscope/pimp-my-box#changing-configuration-defaults
#
#############################################################################

# Root directory of this instance (RT_HOME is set in the "start" script).
# With vanilla rTorrent, must be changed to something like…
#   … string, (cat,"/var/torrent/")
method.insert = cfg.basedir, private|const|string, (cat,(system.env,"RT_HOME"),"~/.rtorrent/")

# Set "pyro.extended" to 1 to activate rTorrent-PS features!
method.insert = pyro.extended, const|value, 1

# Set "pyro.bin_dir" to the "bin" directory where you installed the pyrocore tools!
# Make sure you end it with a "/"; if this is left empty, then the shell's path is searched.
method.insert = pyro.bin_dir, const|string, ~/.local/bin/

# Since "network.scgi.open_local" is parsed by "rtcontrol", this must be a literal value,
# and also not moved out of the main configuration file!
scgi_port = 127.0.0.1:5000

# SCHEDULE: Make SCGI socket group-writable and secure
schedule2 = scgi_permission, 0, 0, "execute.nothrow=chmod,\"g+w,o=\",/tmp/scgi.socket"

#
# Include custom / optional settings
#

# INCLUDE: Local settings
import = (cat,(cfg.basedir),"_rtlocal.rc")

# INCLUDE: ruTorrent (optional)
try_import = (cat,(cfg.basedir),"_rutorrent.rc")

### END rtorrent.rc #########################################################
