#! /usr/bin/env bash

bspwm_dir="$(dirname $(readlink -f $0))"
export HOSTNAME="$(hostname)"

if [ "$HOSTNAME" = "temeraire" ]; then

  # monitor settings -{{{
    for monitor in $(bspc query -M); do
        bspc monitor $monitor -d $monitor/{1,2,3,4,5}
    done

  # }}}-

  # bspwm setting -{{{
    bspc config border_width 7
    bspc config split_ratio  0.50

    bspc config borderless_monocle        true
    bspc config gapless_monocle           true
    bspc config focus_follows_pointer     true
    bspc config center_pseudo_tiled       true
    bspc config remove_unplugged_monitors true

    bspc config normal_border_color   "#1f2021"
    bspc config focused_border_color  "#413235"
    bspc config presel_feedback_color "#26262b"

  # }}}-

  # autostarting -{{{
    ${bspwm_dir}/rules &

    polybar left &
    polybar right &
    polybar main-top &
  # }}}-

elif [ "$HOSTNAME" == "perscitia" ]; then
  # bspwm setting -{{{
    bspc config border_width 2
    bspc config split_ratio 0.50

    bspc config top_padding    0
    bspc config left_padding   0
    bspc config right_padding  0
    bspc config bottom_padding 0
    bspc config window_gap     0

    bspc config borderless_monocle        true
    bspc config gapless_monocle           true
    bspc config focus_follows_pointer     true
    bspc config remove_unplugged_monitors true

    bspc config normal_border_color   "#1f1d21"
    bspc config focused_border_color  "#565e6b"
    bspc config presel_feedback_color "#32a37c"
  # }}}-

  # monitor settings -{{{
    i=1
    for monitor in $(bspc query -M); do
        bspc monitor $monitor \
            -n "$i" \
            -d $i/{i,ii,iii,iv,v}
        let i++
    done
    unset i
  # }}}-

  # autostarting -{{{
    feh --bg-fill ~/pRq.jpg
    xsetroot -cursor_name left_ptr
    export SXHDK_SHELL=dash

    sxhkd -c ~/.config/sxhkd/sxhkdrc.laptop &
    compton --config ~/.config/compton.laptop.conf &
    dunst -config ~/.config/dunst/dunstrc.laptop &
    unclutter &
    redshift &

    cd ~/.config/lemonbar
    #./panel &
    cd ~
  # }}}-

fi
